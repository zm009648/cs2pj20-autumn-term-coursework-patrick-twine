package drone;

public class ConsoleCanvas {
	
	private char[][] Arena; // 2D array used to store info pertaining to arena
	private double x, y;
	
	public ConsoleCanvas(DroneArena a) { // Constructor that creates a blank canvas
		int i, j;
		x = (int) (a.SizeX+3); // SizeN + 3 to account for both top and bottom and also starting at index 0
		y = (int) (a.SizeY+3);
		Arena = new char[(int) x][(int) y];
		for (i = 0; i < x; i++) {
			for (j = 0; j < y; j++) {
				if (i == 0 || i == x - 1) {
					Arena[i][j] = '#'; // Apparently char only works with ' instead of "
				}
				else if ( j == 0 || j == y - 1) {
					Arena[i][j] = '#';
				}
				else {
					Arena[i][j] = ' ';
				}
			}
		}
	}
	
	public void addDrone(double PosX, double PosY) { // Will only add one drone, has to be run once per drone in Droneslist
		Arena[(int) PosX+1][(int) PosY+1] = 'D'; // 
	}
	
	public void addAllDrones(DroneArena a) {
		int i = 0;
		int j = 0;
		x = a.SizeX+3;
		y = a.SizeY+3;
		for (i = 0; i < x; i++) {
			for (j = 0; j < y; j++) {
				if (Arena[i][j] == 'D') {
					Arena[i][j] = ' ';
				}
			}
		}
		i = 0;
		for (i = 0; i < a.Droneslist.size(); i++) {
			Arena[(int) (a.Droneslist.get(i).getPosX()+1)][(int) (a.Droneslist.get(i).getPosY()+1)] = 'D';
		}
	}
	
	public String showArena() {
		String buf = "";
		for (int i = (int) (x-1); i >= 0; i--) {
			for (int j = 0; j < y; j++) {
				buf += Arena[i][j] + "";
			}
			buf += "\n";
		}
		return buf;
	}
}
