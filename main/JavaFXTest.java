package drone;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class JavaFXTest extends Application { // Should be DroneInterface, but that doesnt work for some reason
	private int xCanvasSize = 400, yCanvasSize = 500;	// size of canvas
    private GUICanvas mc; 								// canvas in which system drawn
    private boolean animationOn = false;						// are we animating?
    private VBox rtPane;								// pane in which info on drone listed
    public DroneArena GUIDroneArena = new DroneArena(xCanvasSize, yCanvasSize);
    public int DroneCount;
    

	 /**
	  * Function to show a message, 
	  * @param TStr		title of message block
	  * @param CStr		content of message
	  */
	private void showMessage(String TStr, String CStr) {
		    Alert alert = new Alert(AlertType.INFORMATION);
		    alert.setTitle(TStr);
		    alert.setHeaderText(null);
		    alert.setContentText(CStr);

		    alert.showAndWait();
	}
	 private void showAbout() {
		 showMessage("About", "Patrick Twine's \"Drone\" Simulator");
	 }
	 private void showHelp() {
		 showMessage("Help", "This is a program that simulates the movement of coloured balls, called \"Drones\". " + "\n" +
	 "You can click on the canvas to add a drone on the position you click, or you can press the \"Random Drone\" button to spawn a drone at a random point on the canvas" + "\n" +
	 "Once you are satisfied with the amount of drones, press the \"Start Animation\" button to start the animation.");
	 }
	 
	/**
	 * Function to set up the menu
	 */
	public MenuBar setMenu() {
		MenuBar menuBar = new MenuBar();		// create menu

		Menu mHelp = new Menu("Help");			// have entry for help
					// then add sub menus for About and Help
					// add the item and then the action to perform
		MenuItem mAbout = new MenuItem("About");
		mAbout.setOnAction(new EventHandler<ActionEvent>() {
	           @Override
	           public void handle(ActionEvent actionEvent) {
	            	showAbout();				// show the about message
	           }	
		});
		MenuItem mfHelp = new MenuItem("Help");
		mfHelp.setOnAction(new EventHandler<ActionEvent>() {
	           @Override
	           public void handle(ActionEvent actionEvent) {
	            	showHelp();				// show the about message
	           }	
		});
		mHelp.getItems().addAll(mAbout, mfHelp); 	// add submenu to Help
			
				// now add File menu, which here only has Exit
		Menu mFile = new Menu("File");				// create File Menu
		MenuItem mExit = new MenuItem("Exit");		// and Exit submenu
		mExit.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent t) {		// and add handler
		        System.exit(0);						// quit program
		    }
		});
		mFile.getItems().addAll(mExit);	// add Exit submenu to File
			
		menuBar.getMenus().addAll(mFile, mHelp);	// menu has File and Help
			
		return menuBar;					// return the menu, so can be added
	}
	
	/**
	 * show where ball is, in pane on right
	 */
	public void drawStatus() {
		StringBuffer buf = new StringBuffer();
		buf.append("Amount of Drones: ");
		buf.append(DroneCount);
		rtPane.getChildren().clear();					// clear rtpane
		Label l = new Label(buf.toString());
		rtPane.getChildren().add(l);
	}
	
	public double getDroneCount() {
		return DroneCount;
	}

	public void setDroneCount(int droneCount) {
		DroneCount = droneCount;
	}
	
	public void decreaseDroneCount(int decrease) {
		DroneCount--;
	}

	/**
		 * set up the mouse event handler, so when click on canvas, put ball there
		 * @param canvas
		 */
		private void setMouseEvents (Canvas canvas) {
		       canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, 
		    	       new EventHandler<MouseEvent>() {
		    	           @Override
		    	           public void handle(MouseEvent e) {	    	        	   
		    	        	   GUIDroneArena.createDroneAtPos(GUIDroneArena, e.getX(), e.getY());
		    	        	   DroneCount++;
		    	    			for (Drone d : GUIDroneArena.Droneslist) {
		    	    				d.drawWorld(mc);
		    	    			}
		    		        	drawStatus();					// update panel
		    	           }
		    	       });
		}

		/**
		 * set up the buttons and return so can add to borderpane
		 * @return
		 */
	    private HBox setButtons() {
	    			// create button
	    	Button btnBottom = new Button("Random Drone");
	    			// now add handler
	    	btnBottom.setOnAction(new EventHandler<ActionEvent>() {
	    		@Override
	    		public void handle(ActionEvent event) {
	    			GUIDroneArena.createDrone(GUIDroneArena);
	    			DroneCount++;
	    			for (Drone d : GUIDroneArena.Droneslist) {
	    				d.drawWorld(mc);
	    				
	    			}
	    			drawStatus();
	    		}
	    	});
	    	
	    	Button btnAnimOn = new Button("Start");
			// now add handler
	    	btnAnimOn.setOnAction(new EventHandler<ActionEvent>() {
	    		@Override
	    		public void handle(ActionEvent event) {
	    			animationOn = true;
	    		}
	    	});
	    	
	    	Button btnAnimOff = new Button("Stop");
			// now add handler
	    	btnAnimOff.setOnAction(new EventHandler<ActionEvent>() {
	    		@Override
	    		public void handle(ActionEvent event) {
	    			animationOn = false;
	    		}
	    	});
	    	
	    	return new HBox(btnBottom, new Label(" Animation: "), btnAnimOn, btnAnimOff);
	    }
		
    
	@Override
	public void start(Stage stagePrimary) throws Exception {
		stagePrimary.setTitle("Drone Simulation");

	    BorderPane bp = new BorderPane();			// create border pane

	    bp.setTop(setMenu());						// create menu, add to top

	    Group root = new Group();					// create group
	    Canvas canvas = new Canvas( xCanvasSize, yCanvasSize );
	    											// and canvas to draw in
	    root.getChildren().add( canvas );			// and add canvas to group
	    mc = new GUICanvas(canvas.getGraphicsContext2D(), GUIDroneArena);
					// create MyCanvas passing context on canvas onto which images put
	    GUIDroneArena = new DroneArena(xCanvasSize, yCanvasSize);
	    
	    bp.setCenter(root);							// put group in centre pane

	    rtPane = new VBox();						// set vBox for listing data
	    bp.setRight(rtPane);						// put in right pane

	    bp.setBottom(setButtons());					/// add button to bottom
	    setMouseEvents(canvas);						// set mouse handler
	    

		// for animation, note start time
	    new AnimationTimer()			// create timer
    	{
    		public void handle(long currentNanoTime) {
    				// define handle for what do at this time
    			if (animationOn) {
    				mc.clearCanvas();
    				for (Drone d : GUIDroneArena.Droneslist) {
    					d.updateDrone(mc, GUIDroneArena);			// find new position of drone 
    					d.drawWorld(mc);				// draw drone in new position
    				}
    				
	    			drawStatus();
   			}	
    		}
    	}.start();					// start it

	    Scene scene = new Scene(bp, xCanvasSize*1.5, yCanvasSize*1.2);
	    								// create scene so bigger than canvas, 

	    stagePrimary.setScene(scene);
	    stagePrimary.show();

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Application.launch(args);			// launch the GUI

	}
}
