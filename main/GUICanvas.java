package drone;

import javafx.application.Application;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class GUICanvas extends Application {
	private int xCanvasSize = 250;				// constants for relevant sizes
	private int yCanvasSize = 250;
    GraphicsContext gc; 
    
    /**
     * Constructor sets up relevant Graphics context and size of canvas
     * @param g
     * @param cs
     */
    public GUICanvas(GraphicsContext g, DroneArena a) {
    	gc = g;
    	xCanvasSize = a.SizeX;
    	yCanvasSize = a.SizeY;
    	
    }
    /**
     * get size in x of canvas
     * @return xsize
     */
    public int getXCanvasSize() {
    	return xCanvasSize;
    }
    /**
     * get size of xcanvas in y    
     * @return ysize
     */
    public int getYCanvasSize() {
    	return yCanvasSize;
    }

    /**
     * clear the canvas
     */
    public void clearCanvas() {
		gc.clearRect(0,  0,  xCanvasSize,  yCanvasSize);		// clear canvas
    }
    
	/**
     * drawIt ... draws object defined by given image at position and size
     * @param i		image
     * @param x		xposition	in range 0..1
     * @param y
     * @param sz	size
     */
	public void drawIt (Image i, double x, double y, double sz) {
			// to draw centred at x,y, give top left position and x,y size
			// sizes/position in range 0..1, so scale to canvassize 
		gc.drawImage(i, xCanvasSize * (x - sz/2), yCanvasSize*(y - sz/2), xCanvasSize*sz, yCanvasSize*sz);
	}

	
	Color colourFromSpeed (int Vel){ // Takes velocity of drone and uses it to determine a colour.
		Color ans = Color.BLACK;
		switch (Vel) {
		case 0 : ans = Color.BLACK;
			break;
		case 1 : ans = Color.BLUE;
			break;
		case 2 : ans = Color.GREEN;
			break;			
		case 3 : ans = Color.YELLOW;
			break;
		case 4 : ans = Color.ORANGE;
			break;
		case 5 : ans = Color.RED;
			break;			
		}
		return ans;
	}
	
	public void setFillColour (Color c) {
		gc.setFill(c);
	}
	public void showCircle(double x, double y, double rad, int Vel) {
	 	setFillColour(colourFromSpeed(Vel));									// set the fill colour
		gc.fillArc(x-rad, y-rad, rad*2, rad*2, 0, 360, ArcType.ROUND);	// fill circle
	}

	/**
	 * show circle in current colour atx,y size rad
	 * @param x
	 * @param y
	 * @param rad
	 */
	public void showCircle(double x, double y, double rad) {
		gc.fillArc(x-rad, y-rad, rad*2, rad*2, 0, 360, ArcType.ROUND);	// fill circle
	}

	/**
	 * Show Text .. by writing string s at position x,y
	 * @param x
	 * @param y
	 * @param s
	 */
	public void showText (double x, double y, String s) {
		gc.setTextAlign(TextAlignment.CENTER);							// set horizontal alignment
		gc.setTextBaseline(VPos.CENTER);								// vertical
		gc.setFill(Color.WHITE);										// colour in white
		gc.fillText(s, x, y);						// print score as text
	}

	/**
	 * Show Int .. by writing int i at position x,y
	 * @param x
	 * @param y
	 * @param i
	 */
	public void showInt (double x, double y, int i) {
		showText (x, y, Integer.toString(i));
	}	
	@Override
	public void start(Stage arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}	
}