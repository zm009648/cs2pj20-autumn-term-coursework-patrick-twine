package drone;

public class Drone {
	
	int DroneCount = 0;
	private int Velocity;
	private double PosX, PosY;
	public int ID; 
	private double Direction; // Direction is the direction in which the drone is travelling/facing.  
	
	// Drone Constructor
	public Drone(int Velocity, double PosX, double PosY, int ID, double Direction) {
		this.Velocity = Velocity;
		this.setPosX(PosX);
        this.setPosY(PosY);
        this.ID = ID;
        this.Direction = Direction;
    }
	
// Getters and Setters
	public double getPosX() {
		return PosX;
	}

	public void setPosX(double posX) {
		PosX = posX;
	}
	
	public double getPosY() {
		return PosY;
	}

	public void setPosY(double posY) {
		PosY = posY;
	}
	
	public int getVelocity() {
		return Velocity;
	}
	
	public int getID() {
		return ID;
	}

	public void returnDronePos() { // StringBuffer that says "Drone $DroneCount has position X: $PosX Y: $PosY\n"
        StringBuffer buf = new StringBuffer();
        buf.append("Drone ");
        buf.append(ID);
        buf.append(" has position X: ");
        buf.append(getPosX());
        buf.append(" Y: ");
        buf.append(getPosY());
        buf.append("\n");
        
        if (Velocity == 0) {
        	buf.append("Drone is Stationary ");
        }
        System.out.println(buf);
        System.out.println();
        
    }
	
	public void turnDrone (double newDir) { // Turns singular drone so direction is as set
		Direction = newDir;
    }
	
	public void setPos(double d, double e) { // Sets drone position
		setPosX(d);
		setPosY(e);
	}
	
	public void destroyDrone(DroneArena a, int givenID) { // Doesn't actually destroy drone
		int i = 0;
		for (i = 0; i < a.Droneslist.size(); i++) {
			if (getID() == givenID) {
				System.out.println(i);
				System.out.println(givenID);
				Velocity = 0;
				setPosX((getID()+1)*-25); // Puts drone in corner where it wont be rendered
				setPosY((getID()+1)*-25);
			}
		}
	}

	public void checkDrone(GUICanvas mc, DroneArena a) {
		if (getPosX() < 10 || getPosX() > mc.getXCanvasSize() - 10) Direction = 180-Direction;
		if (getPosY() < 10 || getPosY() > mc.getYCanvasSize() - 10) Direction = 360-Direction;
		if (Direction > 360) {
			Direction = Direction - 360;
		}
		else if (Direction < 0) {
			Direction = Direction + 360;
		}
		int i = 0;
		for (i = 0; i < a.Droneslist.size(); i++) { //If drone is in area surrounding other drone, destroy both
			// If drones are within 2*radius of each other, destroy them both, and reduce number of things in arena
			if (getID() != a.Droneslist.get(i).getID()) { // Ensures drone is not checking to see if it is colliding with itself
				if (Math.sqrt(Math.pow((a.Droneslist.get(i).getPosX() - getPosX()), 2) + Math.pow((a.Droneslist.get(i).getPosY() - getPosY()), 2)) < 20) {
					// If distance between centres of drones is less than 2x radius - ie: drones are touching
					if ((Direction > 45 && Direction < 135) || (Direction > 225 && Direction < 315)) { // If drone is going Left or right
						Direction = 180-Direction;  // Make drone change direction
						if (Direction > 360) {
							Direction = Direction - 360;
						}
						else if (Direction < 0) {
							Direction = Direction + 360;
						}
					}
					else {
						Direction = 360-Direction;
						if (Direction > 360) {
							Direction = Direction - 360;
						}
						else if (Direction < 0) {
							Direction = Direction + 360;
						}
					}
				}
			}
		}
			
		
	}
	
	public void adjustDrone() {
		double radAngle = Direction*Math.PI/180;	// put angle in radians
		setPosX(getPosX() + Velocity * Math.cos(radAngle));		// new X position
		setPosY(getPosY() + Velocity * Math.sin(radAngle));		// new Y position
	}
	
	public void updateDrone(GUICanvas mc, DroneArena a) {
		checkDrone(mc, a);			// check if about to hit side of wall, adjust angle if so
		adjustDrone();			// calculate new position
	}
	
	public void drawWorld(GUICanvas mc) {
		mc.showCircle(getPosX(), getPosY(), 10, getVelocity());
	}	
	
	public void clearWorld(GUICanvas mc) {
		ID = 0;
		mc.clearCanvas();
	}
}
