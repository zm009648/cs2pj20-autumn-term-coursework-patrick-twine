package drone;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class DroneArena {
	
	int SizeX, SizeY;
	private int ID;
	ArrayList<Drone> Droneslist;
	
	public DroneArena(int SizeX, int SizeY) {
        this.SizeX = SizeX;
        this.SizeY = SizeY;
        Droneslist = new ArrayList<Drone>();
    }
	
	public void createDrone(DroneArena a) {
		double X = ThreadLocalRandom.current().nextInt(11, (int) this.SizeX-11); 
		double Y = ThreadLocalRandom.current().nextInt(11, (int) this.SizeY-11);
		double Direction = ThreadLocalRandom.current().nextInt(0, 360);
        int Velocity = ThreadLocalRandom.current().nextInt(0, 6); 
        Drone e = new Drone(Velocity, X, Y, ID, Direction);
        ID++; // Increments ID Counter by 1
        Droneslist.add(e);
    }
	
	public void createDroneAtPos(DroneArena a, double X, double Y) {
		double Direction = ThreadLocalRandom.current().nextInt(0, 360);
		int Velocity = ThreadLocalRandom.current().nextInt(0, 6); 
        Drone e = new Drone(Velocity, X, Y, ID, Direction);
        ID++; // Increments ID Counter by 1
        Droneslist.add(e);
	}
}
