package drone;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class DroneArena {
	
	int SizeX, SizeY, ID, PillarID;
	ArrayList<Drone> Droneslist;
	ArrayList<Pillar> PillarList;
	
	public DroneArena(int SizeX, int SizeY) {
        this.SizeX = SizeX;
        this.SizeY = SizeY;
        Droneslist = new ArrayList<Drone>();
        PillarList = new ArrayList<Pillar>();
    }
	
	public void createDrone(DroneArena a) {
        int X = ThreadLocalRandom.current().nextInt(1, this.SizeX); 
        int Y = ThreadLocalRandom.current().nextInt(1, this.SizeY);
        int Direction = ThreadLocalRandom.current().nextInt(0, 8);
        int Velocity = 1; 
        Drone e = new Drone(Velocity, X, Y, ID, Direction);
        e.returnDronePos();
        ID++; // Increments ID Counter by 1
        Droneslist.add(e);
    }
	
	
	public boolean moveSafety (int x, int y) { // Returns true if safe to move, returns false if unable to move due to border
    	// Check to see what is at requested square, returns relevant safety
    	if (x < 0 || x >= this.SizeX || y < 0 || y >= this.SizeY) { // Checks to see if at border
    		return false;
    	}
		return true; // Function ends on return so will only return true if safe
    }
	
	public void moveDrones(DroneArena a) {
		for (Drone d : a.Droneslist) {
			d.tryMove(a);
		}
		System.out.println("");
	}
	
	public void createPillar(DroneArena a) {
        int X = ThreadLocalRandom.current().nextInt(1, a.SizeX); 
        int Y = ThreadLocalRandom.current().nextInt(1, a.SizeY);
        Pillar e = new Pillar(X, Y, PillarID);
        PillarID++; // Increments ID Counter by 1
        a.PillarList.add(e);
    }
	
	
	
	public static void main(String[] args) {
		DroneArena a = new DroneArena(10, 20);
		ConsoleCanvas can = new ConsoleCanvas(a);
		
		a.createDrone(a);
		a.createDrone(a);
		a.createDrone(a);
		a.createPillar(a);
		can.addAllDrones(a);
		can.addPillars(a);
		System.out.println(can.showArena());
		a.moveDrones(a);
		can.addAllDrones(a);
		System.out.println(can.showArena());
		
		ConsoleCanvas can2 = new ConsoleCanvas(a);
		
		can2.addPillars(a);
		System.out.println(can2.showArena());
		
	}
}
