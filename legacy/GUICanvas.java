package drone;

import javafx.application.Application;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

public class GUICanvas extends Application {
	private int canvasWidth, canvasHeight;
	GraphicsContext gc;
//	Image droneImage = new Image("C:/Users/Student/Documents/Java/Year 2/DroneImage.png");
	
	public GUICanvas(GraphicsContext g, int CanvasX, int CanvasY) {
		gc = g;
		canvasWidth = CanvasX;
		canvasHeight = CanvasY;
	}
	
	public void setFillArenaColour(int CanvasWidth, int CanvasHeight) {
        gc.setFill(Color.GRAY);
        gc.fillRect(0,0, CanvasWidth, CanvasHeight);
        gc.setStroke(Color.GRAY);
        gc.strokeRect(0,0, CanvasWidth, CanvasHeight);
    }
	
	public void changeCanvas(){
        gc.clearRect(0,0, canvasWidth, canvasHeight);
        setFillArenaColour(canvasWidth, canvasHeight);
    }
	
	public void newCanvas() {
		gc.clearRect(0,  0,  canvasWidth,  canvasHeight);		// clear canvas
    }
	
	public void createDrone (DroneArena a) {
		for (Drone d : a.Droneslist) {
			gc.drawImage(droneImage, d.PosX, d.PosY, 20, 20);
		}
}
	
	public void showDrone(int x, int y) {
		gc.fillArc(x-5, y-5, 5*2, 5*2, 0, 360, ArcType.ROUND);	// fill circle
	}

	@Override
	public void start(Stage arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
}