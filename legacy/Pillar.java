package drone;

public class Pillar {
	
	int PosX, PosY;
	public int PillarID; 
	
	public Pillar(int PosX, int PosY, int PillarID) {
		this.PosX = PosX;
        this.PosY = PosY;
        this.PillarID = PillarID;
	}
}
