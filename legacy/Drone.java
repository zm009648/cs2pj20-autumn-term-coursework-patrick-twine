package drone;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Drone {
	
	int DroneCount = 0;
	int Velocity;
	int PosX, PosY;
	public int ID; 
	int Direction; // Direction is the direction in which the drone is travelling/facing.  
	
	// Drone Constructor
	public Drone(int Velocity, int PosX, int PosY, int ID, int Direction) {
		this.Velocity = Velocity;
		this.PosX = PosX;
        this.PosY = PosY;
        this.ID = ID;
        this.Direction = Direction;
    }
	
	public void returnDronePos() { // StringBuffer that says "Drone $DroneCount has position X: $PosX Y: $PosY\n"
        StringBuffer buf = new StringBuffer();
        buf.append("Drone ");
        buf.append(ID);
        buf.append(" has position X: ");
        buf.append(PosX);
        buf.append(" Y: ");
        buf.append(PosY);
        buf.append("\n");
        
        if (Velocity == 0) {
        	buf.append("Drone is Stationary ");
        }
        System.out.println(buf);
        System.out.println();
        
    }
	
	public void tryMove(DroneArena a) {
		if (Velocity == 0) {
			System.out.print(this.ID);
			System.out.println(" is moving n");
			//If velocity is 0 then it doesn't need to worry about bouncing
		}
		else if (Direction == 0) { // North
			if (a.moveSafety(PosX, PosY+1) == true) {
				PosY++;	// If safe, increase Y position by 1
				System.out.print(this.ID);
				System.out.println(" is moving n");
			} else {
				Direction = 4; // If unsafe, drone should bounce back - 4 = South
			}
		}
		else if (Direction == 1) { // North-East
			if (a.moveSafety(PosX+1, PosY+1) == true) {
				PosY++;
				PosX++;
				System.out.print(this.ID);
				System.out.println(" moving ne");
			} else { // If unsafe, drone should bounce depending on what surface was hit
				if ((a.moveSafety(PosX, PosY+1) == false) && (a.moveSafety(PosX+1, PosY) == false)) { // If both unsafe, bounce back South-West
					Direction = 5;
				}
				else if (a.moveSafety(PosX, PosY+1) == false) { // If North unsafe, bounce back South-East
					Direction = 3;
				}
				else if (a.moveSafety(PosX+1, PosY) == false) { // If East unsafe, bounce back North-West
					Direction = 7;
				}
			}
		}
		else if (Direction == 2) { // East
			if (a.moveSafety(PosX+1, PosY) == true) {
				PosX++;
				System.out.print(this.ID);
				System.out.println(" moving e");
			} else { 
				if (a.moveSafety(PosX+1, PosY) == false) { // If East unsafe, bounce back West
					Direction = 6;
				}
			}
		}
		else if (Direction == 3) { // South-East
			if (a.moveSafety(PosX+1, PosY-1) == true) {
				PosY--;
				PosX++;
				System.out.print(this.ID);
				System.out.println(" moving se");
			} else { 
				if ((a.moveSafety(PosX, PosY-1) == false) && (a.moveSafety(PosX+1, PosY) == false)) { // If unsafe, bounce back North-West
					Direction = 7;
				}
				else if (a.moveSafety(PosX, PosY-1) == false) { // If South unsafe, bounce back North-East
					Direction = 1;
				}
				else if (a.moveSafety(PosX+1, PosY) == false) { // If East unsafe, bounce back South-West
					Direction = 3;
				}
			}
		}
		else if (Direction == 4) { // South
			if (a.moveSafety(PosX, PosY-1) == true) {
				PosX++;
				System.out.print(this.ID);
				System.out.println(" moving s");
			} else { 
				if (a.moveSafety(PosX, PosY-1) == false) { // If South unsafe, bounce back North
					Direction = 0;
				}
			}
		}
		else if (Direction == 5) { // South-West
			if (a.moveSafety(PosX-1, PosY-1) == true) {
				PosY--;
				PosX--;
				System.out.print(this.ID);
				System.out.println(" moving sw");
			} else { 
				if ((a.moveSafety(PosX, PosY-1) == false) && (a.moveSafety(PosX-1, PosY) == false)) { // If both unsafe, bounce back North-East
					Direction = 1;
				}
				else if (a.moveSafety(PosX, PosY-1) == false) { // If South unsafe, bounce back North-West
					Direction = 7;
				}
				else if (a.moveSafety(PosX-1, PosY) == false) { // If West unsafe, bounce back South-East
					Direction = 3;
				}
			}
		}
		else if (Direction == 6) { // West
			if (a.moveSafety(PosX-1, PosY) == true) {
				PosX--;
				System.out.print(this.ID);
				System.out.println(" moving w");
			} else { 
				if (a.moveSafety(PosX-1, PosY) == false) { // If West unsafe, bounce back East
					Direction = 2;
				}
			}
		}
		else if (Direction == 7) { // North-West
			if (a.moveSafety(PosX-1, PosY+1) == true) {
				PosY++;
				PosX--;
				System.out.print(this.ID);
				System.out.println(" moving nw");
			} else { 
				if ((a.moveSafety(PosX, PosY+1) == false) && (a.moveSafety(PosX-1, PosY) == false)) { // If both unsafe, bounce back North-East
					Direction = 1;
				}
				else if (a.moveSafety(PosX, PosY+1) == false) { // If North unsafe, bounce back South-West
					Direction = 5;
				}
				else if (a.moveSafety(PosX-1, PosY) == false) { // If West unsafe, bounce back North-East
					Direction = 1;
				}
			}
		}
	}

	public void turnDrones(DroneArena a) { // Makes given drone turn
		Random random = new Random();
		int newDir;
		boolean willTurn;
		newDir = ThreadLocalRandom.current().nextInt(0, 9);
		willTurn = random.nextBoolean();
		if (willTurn) { // If willTurn is true then drone will get new random velocity 
			Direction = newDir;
		} 
	}
	
	public void turnDrone (int newDir) { // Turns singular drone
		Direction = newDir;
    }
	
	public void deleteDrones() {
		ID = 0;
	}

	
}
