package drone;

import java.util.ArrayList;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/* FUCK IT PLANNING HERE
 * Main functionality comes by just straightup fucking printing ConsoleCanvas here
 * 		when shit moves the gui should be wiped every time it moves
 * Adds drones via addDrone in ConsoleCanvas but add a button for the extra sodding marks
 */

 // ok so tonight lets just get it so you define the size of the arena and then it creates the canvas
 // great thats done, lets move onto getting the canvas on screen

public class JavaFXTest extends Application { // Should be DroneInterface, but that doesnt work for some reason
	
	private DroneArena a;
	VBox vbox;
	private GUICanvas GUIC;
	private TextArea objectText;
	private int X, Y;
	private static AnimationTimer time;
	private Canvas canvas;
	private BorderPane pane;
	
	
	public void start(Stage primaryStage) throws Exception {
		a = new DroneArena(400,500);
        
        primaryStage.setTitle("Drone Simulation");
        
        BorderPane pane = new BorderPane();
        
        Canvas canvas = new Canvas(400, 500);
        Group group = new Group();
        
        group.getChildren().add(canvas);
        pane.setLeft(group);
        
        GUIC = new GUICanvas(canvas.getGraphicsContext2D(), 400, 500);
        newDroneCanvas();
        
        vbox = new VBox();  //declares a new virtual box
        pane.setRight(vbox); //places pane right of virtual box
        vbox.setPadding(new Insets(5, 75, 75, 5));
        pane.setRight(vbox);
        
        Scene scene = new Scene(pane, 640, 480);
        
        pane.setLeft(startMenu(pane));

        primaryStage.setScene(scene);
        primaryStage.show();

    }
	
	private HBox createButtons() {
		Button spawnDrone = new Button("Spawn Drone");
		spawnDrone.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent spawnDrone)  
			{
				a.createDrone(a); 
//				addToObjectList();
			}
		});
		
		Button moveDrones = new Button("Move Drones");
		moveDrones.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent moveDrones)  
			{
				a.moveDrones(a);
			}
		});
		
		Button showDroneCanvas = new Button("Show Arena");
		showDroneCanvas.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent showDroneCanvas)  
			{
				GUIC = new GUICanvas(canvas.getGraphicsContext2D(), 400, 500);
		        a = new DroneArena(400,500);
		        GUIC.setFillArenaColour(Y, X);
			}
		});
		
		Button showDroneConsole = new Button("Show Arena in Console");
		showDroneConsole.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent showDroneConsole)  
			{
				ConsoleCanvas c = new ConsoleCanvas(a);
				c.addAllDrones(a);
				System.out.println(c.showArena());  
			}
		});
		
		return new HBox(spawnDrone, moveDrones, showDroneCanvas, showDroneConsole);
	}

	
	public HBox startMenu(BorderPane pane) {
		TextField xInput = new TextField();
		TextField yInput = new TextField();
		xInput.setText("10");
		yInput.setText("20");
        
        Button returnSize = new Button("Arena Size Entered");
        
        returnSize.setOnAction(action -> {
            System.out.println(xInput.getText());
            System.out.println(yInput.getText());
            
            String text = xInput.getText();
            X = Integer.parseInt(text);
            text = yInput.getText();
            Y = Integer.parseInt(text);
            
            a = new DroneArena(X,Y);
            
            pane.setLeft(createButtons());
            pane.setRight(objectList());
        });
        
        return new HBox(xInput, yInput, returnSize);
	}
    
    public void newDroneCanvas() {
    	// Create canvas, add all drones to canvas, display canvas
    	GUIC.newCanvas();
    }
    
    public void addToObjectList() {
    	StringBuffer overall = new StringBuffer();
    	for (Drone d : a.Droneslist) {
    		StringBuffer buf = new StringBuffer();
            buf.append("Drone ");
            buf.append(d.ID);
            buf.append(" has position X: ");
            buf.append(d.PosX);
            buf.append(" Y: ");
            buf.append(d.PosY);
            buf.append("\n"); 
            overall.append(buf);
    	}
    	String output = overall.toString();
//    	objectText.setEditable(true);
//    	TextArea objectText = new TextArea();
    	System.out.println(output);
    	objectText.setText(output);
//    	return new HBox(objectText);
    	
//    	objectText.setEditable(false);
    	// Stringbuffer gets added to label
    	// Label should be made as soon as the buttons are made
    }
    
    public HBox objectList() {
    	// End goal is textarea on right side with full list of all the shit gotten from droneData
    	TextArea objectText = new TextArea(); // Define a textarea
//    	objectText.setEditable(false);
		return new HBox(objectText);
    }
    
    
    public static void main(String[] args) {
        launch(args);
    }
}