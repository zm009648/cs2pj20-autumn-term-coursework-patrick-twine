package drone;

public class ConsoleCanvas {
	
	char[][] Arena; // 2D array used to store info pertaining to arena
	int x, y;
	
	public ConsoleCanvas(DroneArena a) { // Constructor that creates a blank canvas
		int i, j;
		x = a.SizeX+3; // SizeN + 3 to account for both top and bottom and also starting at index 0
		y = a.SizeY+3;
		Arena = new char[x][y];
		for (i = 0; i < x; i++) {
			for (j = 0; j < y; j++) {
				if (i == 0 || i == x - 1) {
					Arena[i][j] = '#'; // Apparently char only works with ' instead of "
				}
				else if ( j == 0 || j == y - 1) {
					Arena[i][j] = '#';
				}
				else {
					Arena[i][j] = ' ';
				}
			}
		}
	}
	
	public void addDrone(int PosX, int PosY) { // Will only add one drone, has to be run once per drone in Droneslist
		Arena[PosX+1][PosY+1] = 'D'; // 
	}
	
	public void addAllDrones(DroneArena a) {
		int i = 0;
		int j = 0;
		x = a.SizeX+3;
		y = a.SizeY+3;
		for (i = 0; i < x; i++) {
			for (j = 0; j < y; j++) {
				if (Arena[i][j] == 'D') {
					Arena[i][j] = ' ';
				}
			}
		}
		i = 0;
		for (i = 0; i < a.Droneslist.size(); i++) {
			Arena[a.Droneslist.get(i).PosX+1][a.Droneslist.get(i).PosY+1] = 'D';
		}
	}
	
	public void addPillars(DroneArena a) {
		int i = 0;
		x = a.SizeX+3;
		y = a.SizeY+3;
		for (i = 0; i < a.PillarList.size(); i++) {
			Arena[a.PillarList.get(i).PosX+1][a.PillarList.get(i).PosY+1] = 'P';
		}
	}
	
	public String showArena() {
		String buf = "";
		for (int i = x-1; i >= 0; i--) {
			for (int j = 0; j < y; j++) {
				buf += Arena[i][j] + "";
			}
			buf += "\n";
		}
		return buf;
	}
	
	
	public static void main(String[] args) {
		DroneArena a = new DroneArena(12, 20);
		ConsoleCanvas can = new ConsoleCanvas(a);
		can.addDrone(6,2);
		can.addDrone(6,0);
		can.addDrone(6,8);
		can.addDrone(0,4);
		can.addDrone(12,5);
		System.out.println(can.showArena()); 
	}
}
